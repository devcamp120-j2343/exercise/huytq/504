import { Employee } from "./employee.js";

class CommissionEmployee extends Employee {
    grossSales;
    commissionRate;

    constructor(paramFirstName, paramLastName, paramSocialSecurityNumber, paramGrossSales, paramCommissionRate) {
        super(paramFirstName, paramLastName, paramSocialSecurityNumber);
        this.grossSales = paramGrossSales;
        this.commissionRate = paramCommissionRate;
    }

    setGrossSales(paramGrossSales) {
        this.grossSales = paramGrossSales;
    }

    getGrossSales() {
        return this.grossSales;
    }
}

export { CommissionEmployee }