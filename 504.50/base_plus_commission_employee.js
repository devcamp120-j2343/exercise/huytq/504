import { CommissionEmployee } from "./commission_employee.js";

class BasePlusCommissionEmployee extends CommissionEmployee {
    baseSalary;

    constructor(paramFirstName, paramLastName, paramSocialSecurityNumber, paramGrossSales, paramCommissionRate, paramBaseSalary) {
        super(paramFirstName, paramLastName, paramSocialSecurityNumber, paramGrossSales, paramCommissionRate);
        this.baseSalary = paramBaseSalary;
    }

    setBaseSalary(paramBaseSalary) {
        this.baseSalary = paramBaseSalary;
    }

    getBaseSalary() {
        return this.baseSalary;
    }
}

export { BasePlusCommissionEmployee }