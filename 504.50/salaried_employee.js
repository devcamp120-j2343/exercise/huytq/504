import { Employee } from "./employee.js";

class SalariedEmployee extends Employee {
    wekklySalary;

    constructor(paramFirstName, paramLastName, paramSocialSecurityNumber, paramWeeklySalary) {
        super(paramFirstName, paramLastName, paramSocialSecurityNumber);
        this.wekklySalary = paramWeeklySalary;
    }

    setWeeklySalary(paramWeeklySalary) {
        this.wekklySalary = paramWeeklySalary;
    }

    getWeeklySalary() {
        return this.wekklySalary;
    }
}

export { SalariedEmployee }