import { Employee } from "./employee.js";
import { CommissionEmployee } from "./commission_employee.js";
import { HourlyEmployee } from "./hourly_employee.js";
import { SalariedEmployee } from "./salaried_employee.js";
import { BasePlusCommissionEmployee } from "./base_plus_commission_employee.js";

var Employee1 = new Employee("Huy","Tran","111");
Employee1.setSocialSecurityNumber("999");
console.log(Employee1.getSocialSecurityNumber());
console.log(Employee1 instanceof Employee);

var CommissionEmployee1 = new CommissionEmployee("Huy","Tran","111",1.5,2.5);
CommissionEmployee1.setGrossSales(2.5);
console.log(CommissionEmployee1.getGrossSales());
console.log(CommissionEmployee1 instanceof Employee);

var HourlyEmployee1 = new HourlyEmployee("Huy","Tran","111",1.5,2.5);
HourlyEmployee1.setWage(2.5);
console.log(HourlyEmployee1.getWage());
console.log(HourlyEmployee1 instanceof Employee);

var SalariedEmployee1 = new SalariedEmployee("Huy","Tran","111",1500000);
SalariedEmployee1.setWeeklySalary(1700000.2);
console.log(SalariedEmployee1.getWeeklySalary());
console.log(SalariedEmployee1 instanceof Employee);

var BasePlusCommissionEmployee1 = new BasePlusCommissionEmployee("Huy","Tran","111",1.5,2.5,1000.10);
BasePlusCommissionEmployee1.setBaseSalary(2000.20);
console.log(BasePlusCommissionEmployee1.getBaseSalary());
BasePlusCommissionEmployee1.setSocialSecurityNumber("021"); //cháu kế thừa method của ông
console.log(BasePlusCommissionEmployee1.getSocialSecurityNumber());
console.log(BasePlusCommissionEmployee1 instanceof CommissionEmployee); //con của bố
console.log(BasePlusCommissionEmployee1 instanceof Employee); //cháu của ông
