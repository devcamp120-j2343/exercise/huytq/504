class Employee {
    firstName;
    lastName;
    socialSecurityNumber;

    constructor(paramFirstName, paramLastName, paramSocialSecurityNumber) {
        this.firstName = paramFirstName;
        this.lastName = paramLastName;
        this.socialSecurityNumber = paramSocialSecurityNumber;
    }

    setSocialSecurityNumber(paramSocialSecurityNumber) {
        this.socialSecurityNumber = paramSocialSecurityNumber;
    }
    
    getSocialSecurityNumber() {
        return this.socialSecurityNumber;
    }
}

export { Employee }