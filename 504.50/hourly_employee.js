import { Employee } from "./employee.js";

class HourlyEmployee extends Employee {
    wage;
    hours;

    constructor(paramFirstName, paramLastName, paramSocialSecurityNumber, paramWage, paramHours) {
        super(paramFirstName, paramLastName, paramSocialSecurityNumber);
        this.wage = paramWage;
        this.hours = paramHours;
    }

    setWage(paramWage) {
        this.wage = paramWage;
    }

    getWage() {
        return this.wage;
    }
}

export { HourlyEmployee }