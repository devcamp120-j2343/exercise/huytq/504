
    class Person {
        _personName;
        _personAge;
        _gender;
        constructor(paramName, paramAge, paramGender) {
            this._personName = paramName;
            this._personAge = paramAge;
            this._gender = paramGender;
        }
        getPersonInfo() {
            return "Ten: " + this._personName + " ,Tuoi: " + this._personAge + " ,Gioi tinh: "  + this._gender;
        }
    }
    var person1 = new Person("Huy", "26", "Nam");
    console.log(person1.getPersonInfo());
    console.log(person1 instanceof Person);

    var person2 = new Person("Tuan", "25", "Nam");
    console.log(person2.getPersonInfo());
    console.log(person1 instanceof Person);

    var person3 = new Person("Thy", "24", "Nu");
    console.log(person3.getPersonInfo());
    console.log(person1 instanceof Person);

    export {Person}