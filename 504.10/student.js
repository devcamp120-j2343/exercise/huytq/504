import { Person } from "./person.js";
class Student extends Person {
    _standard;
    _collegeName;
    _grade;
    constructor(paramName, paramAge, paramGender, paramStandard, paramCollegeName, paramGrade){
        super(paramName, paramAge, paramGender);
        this._standard = paramStandard;
        this._collegeName = paramCollegeName;
        this._grade = paramGrade;
}

    getPersonInfo(){
        return "Ten: " + this._personName + " ,Tuoi: " + this._personAge + " ,Gioi tinh: "  + this._gender;
    }

    getStudentInfo(){
        return "Sinh vien ten: " + this._personName + 
        " ,Tuoi: " + this._personAge + 
        " ,Gioi tinh: "  + this._gender +
        " ,Ngành: " + this._standard +
        " ,Trường Đh: " + this._collegeName +
        " ,Điểm: " + this._grade 
    }

    //getter
    getGrade() {
        return this._grade;
    }

    //setter
    setGrade(paramGrade) {
        this._grade = paramGrade;
    }
}
var student1 = new Student("Huy", "26", "Nam","Web Developer","DH SAI GON","7");
console.log(student1.getPersonInfo());
console.log(student1.getStudentInfo());
console.log(student1.getGrade());
student1.setGrade("8");
console.log(student1.getGrade());
console.log(student1.getStudentInfo());
console.log(student1 instanceof Student);

var student2 = new Student("Kiệt", "26", "Nam","Web Developer","DH SAI GON","7");
console.log(student2.getPersonInfo());
console.log(student2.getStudentInfo());
console.log(student2.getGrade());
student2.setGrade("9");
console.log(student2.getGrade());
console.log(student2.getStudentInfo());
console.log(student2 instanceof Student);

var student3 = new Student("Nhi", "26", "Nữ","Web Developer","DH SAI GON","7");
console.log(student3.getPersonInfo());
console.log(student3.getStudentInfo());
console.log(student3.getGrade());
student3.setGrade("6");
console.log(student3.getGrade());
console.log(student3.getStudentInfo());
console.log(student3 instanceof Student);
export {Student}