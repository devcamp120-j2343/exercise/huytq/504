import { Car } from "./car.js";
class ElectricCar extends Car {
    _batteryLevel;
    constructor(paramMake, paramModel, paramBatteryLevel){
        super(paramMake, paramModel);
        this._batteryLevel = paramBatteryLevel;
    }

    drive(){
        return "Hãng: " + this._make + " ,Model: " + this._model ;
    }

    charge(){
        return "Hãng: " + this._make + " ,Model: " + this._model + " ,Sạc" +this._batteryLevel ;
    }

}
var electriccar1 = new ElectricCar("Telsa", "M Sport", "72.7 kwh");
console.log(electriccar1.drive());
console.log(electriccar1.charge())
console.log(electriccar1 instanceof ElectricCar);


export {ElectricCar}