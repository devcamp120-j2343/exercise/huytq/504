
    class Car {
        _make;
        _model;
        
        constructor(paramMake, paramModel) {
            this._make = paramMake;
            this._model = paramModel;
        }
        drive() {
            return "Hãng: " + this._make + " ,Model: " + this._model;
        }
    }
    var car1 = new Car("Tesla", "M Sport");
    console.log(car1.drive());
    console.log(car1 instanceof Car);

    export {Car}