import { Car } from "./car.js";
class PetrolCar extends Car {
    _fuelLevel;
    constructor(paramMake, paramModel, paramFuelLevel){
        super(paramMake, paramModel);
        this._fuelLevel = paramFuelLevel;
    }

    drive(){
        return "Hãng: " + this._make + " ,Model: " + this._model ;
    }

    fillUp(){
        return "Hãng: " + this._make + " ,Model: " + this._model + " ,Dung lượng xăng: " +this._fuelLevel ;
    }

}
var petrolCar = new PetrolCar("Vision", "M Sport", "5.2 L");
console.log(petrolCar.drive());
console.log(petrolCar.fillUp())
console.log(petrolCar instanceof PetrolCar);


export {PetrolCar}