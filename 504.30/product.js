class Product {
    name;
    price;
    numberOfCopies;

    constructor(paramName, paramPrice, paramNumberOfCopies) {
        this.name = paramName;
        this.price = paramPrice;
        this.numberOfCopies = paramNumberOfCopies;
    }

    sellCopies() {
        console.log(this.numberOfCopies);
    }

    orderCopies(num) {
        console.log("Order Copies: " + num);
    }
}

export { Product }