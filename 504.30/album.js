import { Product } from "./product.js";

class Album extends Product {
    artist;

    constructor(paramName, paramPrice, paramNumberOfCopies, paramArtist) {
        super(paramName,paramPrice,paramNumberOfCopies);
        this.artist = paramArtist;
    }

    getAlbumInfo() {
        var albumInfo = {
            name: this.name,
            price: this.price,
            numberOfCopies: this.numberOfCopies,
            artist: this.artist,
        }
        return albumInfo;
    }
}

export { Album }