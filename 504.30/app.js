import { Album } from "./album.js";
import { Movie } from "./movie.js";
import { Product } from "./product.js";

var Product1 = new Product("HellBlazer",300000, 200);
Product1.sellCopies();
Product1.orderCopies(100);
console.log(Product1 instanceof Product);

var Album1 = new Album("Dancing in the dark",1234,100,"Joji");
console.log(Album1.getAlbumInfo());
console.log(Album1 instanceof Product);

var Movie1 = new Movie("Transformer", 1000000, 10000, "Micheal Bay");
console.log(Movie1.getMovieInfo());
console.log(Movie1 instanceof Product);