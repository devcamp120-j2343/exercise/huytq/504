import { Product } from "./product.js";

class Movie extends Product {
    director;

    constructor(paramName, paramPrice, paramNumberOfCopies, paramDirector) {
        super(paramName,paramPrice,paramNumberOfCopies);
        this.director = paramDirector;
    }

    getMovieInfo() {
        var movieInfo = {
            name: this.name,
            price: this.price,
            numberOfCopies: this.numberOfCopies,
            director: this.director,
        }
        return movieInfo;
    }
}

export { Movie }