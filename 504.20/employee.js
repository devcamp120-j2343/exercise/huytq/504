import { Person } from "./person.js";
class Employee extends Person {
    _employer;
    _salary;
    _position;
    constructor(paramName, paramAge, paramGender, paramEmployer, paramSalary, paramPosition){
        super(paramName, paramAge, paramGender);
        this._employer = paramEmployer;
        this._salary = paramSalary;
        this._position = paramPosition;
}

    getPersonInfo(){
        return "Ten: " + this._personName + " ,Tuoi: " + this._personAge + " ,Gioi tinh: "  + this._gender;
    }

    getEmployeeInfo(){
        return "Nhân viên tên: " + this._personName + 
        " ,Tuổi: " + this._personAge + 
        " ,Giới tính: "  + this._gender +
        " ,Công ty: " + this._employer +
        " ,Lương: " + this._salary +
        " ,Vị trí: " + this._position 
    }

    //getter
    getSalary() {
        return this._salary;
    }

    //setter
    setSalary(paramSalary) {
        this._salary = paramSalary;
    }
    //getter
    getPosition() {
        return this._position;
    }

    //setter
    setPosition(paramPosition) {
        this._position = paramPosition;
    }
}
var empolyee1 = new Employee("Huy", "26", "Nam","F Soft","7.000.000 VND","Fresher");
console.log(empolyee1.getPersonInfo());
console.log(empolyee1.getEmployeeInfo());
console.log(empolyee1.getSalary());
console.log(empolyee1.getPosition());
empolyee1.setSalary("8.000.000 VND");
empolyee1.setPosition("Official Employee");
console.log(empolyee1.getEmployeeInfo());
console.log(empolyee1 instanceof Employee);

var empolyee2 = new Employee("Hùng", "26", "Nam","TMA SOLUTION","15.000.000 VND","Team Leader");
console.log(empolyee2.getPersonInfo());
console.log(empolyee2.getEmployeeInfo());
console.log(empolyee2.getSalary());
console.log(empolyee2.getPosition());
empolyee2.setSalary("20.000.000 VND");
empolyee2.setPosition("Product Manager");
console.log(empolyee2.getEmployeeInfo());
console.log(empolyee2 instanceof Employee);

var empolyee3 = new Employee("Hưng", "26", "Nam","TMA SOLUTION","10.000.000 VND","Trainer");
console.log(empolyee3.getPersonInfo());
console.log(empolyee3.getEmployeeInfo());
console.log(empolyee3.getSalary());
console.log(empolyee3.getPosition());
empolyee3.setSalary("12.000.000 VND");
empolyee3.setPosition("Proffesional Trainer");
console.log(empolyee3.getEmployeeInfo());
console.log(empolyee3 instanceof Employee);
export {Employee}